#!/usr/bin/env bash
#
# Deploy a package to PackageCloud.io service
#
# Required globals :
#   PACKAGECLOUD_TOKEN
#   PACKAGECLOUD_REPO
#
# Optional globasl :
#   PACKAGECLOUD_ACTION
#   PACKAGECLOUD_FILE
#   DEBUG

source "$(dirname "$0")/common.sh"

info "Starting pipe execution ..."

# required parameters
PACKAGECLOUD_TOKEN=${PACKAGECLOUD_TOKEN:?'PACKAGECLOUD_TOKEN variable missing.'}
PACKAGECLOUD_REPO=${PACKAGECLOUD_REPO:?'PACKAGECLOUD_REPO variable missing.'}

# optional parameters
PACKAGECLOUD_ACTION=${PACKAGECLOUD_ACTION:="push"}
PACKAGECLOUD_FILE=${PACKAGECLOUD_FILE:="*.tgz"}
DEBUG=${DEBUG:="false"}

# Enable/Disable debug mode
DEBUG_ARGS=""
if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  set -x
  DEBUG_ARGS="--verbose"
fi

if [[ "${PACKAGECLOUD_ACTION}" != "push" ]] && [[ "${PACKAGECLOUD_ACTION}" != "yank" ]]; then
  fail "Action doesn't exist : ${PACKAGECLOUD_ACTION} ! Use 'push' or 'yank'."
fi

# Action push
info 'Deploy a package to https://packagecloud.io'
cmd="/usr/local/bundle/bin/package_cloud ${PACKAGECLOUD_ACTION} ${DEBUG_ARGS} ${PACKAGECLOUD_REPO} ${PACKAGECLOUD_FILE}"
${cmd}

if [[ "${PACKAGECLOUD_ACTION}" == "push" ]]; then
  txt="publish"
elif [[ "${PACKAGECLOUD_ACTION}" == "yank" ]]; then
  txt="destroy"
fi

if [[ "${status}" -eq 0 ]]; then
  success "Package \"$PACKAGECLOUD_FILE\" ${txt}ed successfully to repo : $PACKAGECLOUD_REPO"
else
  fail "Failed to ${txt} package \"$PACKAGECLOUD_FILE\"."
fi
