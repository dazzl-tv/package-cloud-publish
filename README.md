# Bitbucket Pipelines Pipe: Package Cloud Publish

Publish your **deb/rpm/node/python/gem** package to [packagecloud.io](https://packagecloud.io) service.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: dazzl-tv/package-cloud-publish:0.2.0
  variables:
    PACKAGECLOUD_TOKEN: '<string>'
    PACKAGECLOUD_REPO: '<string>'
    # PACKAGECLOUD_ACTION: '<string>' # Optional.
    # PACKAGECLOUD_FILE: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                 | Usage                                                      |
| ------------------------ | ---------------------------------------------------------- |
| PACKAGECLOUD_TOKEN (\*)  | API Token. Permitted to publish a package in Package Cloud |
| PACKAGECLOUD_REPO (\*)   | Name to repository                                         |
| PACKAGECLOUD_ACTION      | Action with package (push, yank). Default: `push`          |
| PACKAGECLOUD_FILE        | File to uploaded. Default: `"\*.tgz"`                      |
| DEBUG                    | Turn on extra debug information. Default: `false`.         |

_(\*) = required variable._

## Details

Use tool ruby (package_cloud)[https://rubygems.org/gems/package_cloud] for publish a
package to server [packagecloud.io](https://packagecloud.io).

By default, the pipe publish a NPM package.

## Prerequisites

Have an account in [packagecloud.io](https://packagecloud.io) and an `API token`.

## Examples

NPM push (node) :

```
script:
  - pipe: dazzl-tv/package-cloud-publish:0.2.0
    variables:
      PACKAGECLOUD_TOKEN: 'esrgv4v4bser45468787987b51dxg51ser'
      PACKAGECLOUD_URL: 'noderepo/firstone'
      PACKAGECLOUD_FILE: 'lib-node-package.tgz'
```

NPM remove (node) :

```
script:
  - pipe: dazzl-tv/package-cloud-publish:0.2.0
    variables:
      PACKAGECLOUD_TOKEN: 'esrgv4v4bser45468787987b51dxg51ser'
      PACKAGECLOUD_URL: 'noderepo/firstone'
      PACKAGECLOUD_ACTION: 'yank'
      PACKAGECLOUD_FILE: 'lib-node-package.tgz'
```

## Support

If you’d like help with this pipe, or you have an issue or feature request,
let us know. The pipe is maintained by developer@adazzl.tv.

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## Todo

Test for package :

* ~~deb~~
* rpm
* ~~node~~
* python
* gem

~~Change text for each action : push/yank~~

## License

Copyright (c) 2018 DazzlTV and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
