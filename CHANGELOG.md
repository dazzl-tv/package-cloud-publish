# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: Add test for DEB package
- minor: Change text when push or yank and add test
- minor: Complete test for NPM package

## 0.1.0

- minor: Add test for DEB package
- minor: Complete test for NPM package

## 0.0.1

- patch: Change version to 0.0.0

