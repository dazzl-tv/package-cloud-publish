FROM ruby:alpine

LABEL author="jeremy.vaillant@dazzl.tv"
LABEL description="Docker image for execute pipe."

RUN apk add --update --no-cache jq bash make g++ \
  && gem install package_cloud

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /

ENTRYPOINT ["/pipe.sh"]
