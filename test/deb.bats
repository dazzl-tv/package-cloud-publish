#!/usr/bin/env bats
#
# Test Package Cloud IO publisher for type package DEB
#

set -e

PACKAGECLOUD_REPO="${PACKAGECLOUD_REPO_DEB}"
DEBUG='false'

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/package-cloud-publish"}

  run docker build -t ${DOCKER_IMAGE} .
}

@test "Publish DEB package success" {
  run docker run \
    -e PACKAGECLOUD_TOKEN="${PACKAGECLOUD_TOKEN}" \
    -e PACKAGECLOUD_REPO="${PACKAGECLOUD_REPO}" \
    -e PACKAGECLOUD_FILE="test/deb-package-test.deb" \
    -e PACKAGECLOUD_ACTION="push" \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "0" ]]
  [[ "${output}" =~ "published successfully to repo" ]]
}

@test "Publish DEB package missing globals TOKEN" {
  run docker run \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "PACKAGECLOUD_TOKEN variable missing." ]]
}

@test "Publish DEB package missing globals REPO" {
  run docker run \
    -e PACKAGECLOUD_TOKEN="${PACKAGECLOUD_TOKEN}" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "PACKAGECLOUD_REPO variable missing." ]]
}

@test "Publish DEB package action error" {
  run docker run \
    -e PACKAGECLOUD_TOKEN="${PACKAGECLOUD_TOKEN}" \
    -e PACKAGECLOUD_REPO="${PACKAGECLOUD_REPO}" \
    -e PACKAGECLOUD_ACTION="123kjfnbk" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "Action doesn't exist" ]]
}

@test "Destroy DEB package" {
  run docker run \
    -e PACKAGECLOUD_TOKEN="${PACKAGECLOUD_TOKEN}" \
    -e PACKAGECLOUD_REPO="${PACKAGECLOUD_REPO}" \
    -e PACKAGECLOUD_FILE=" Fake_1.0.0_all.deb" \
    -e PACKAGECLOUD_ACTION="yank" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "0" ]]
  [[ "${output}" =~ "destroyed successfully to repo" ]]
}
